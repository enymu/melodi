const Discord = require('discord.js');
const client = new Discord.Client();

client.commands = new Discord.Collection();
client.events = new Discord.Collection();

['command_handler', 'event_handler'].forEach(handler =>{
    require(`./handlers/${handler}`)(client, Discord);
})

client.once('ready', () => {

    client.user.setPresence({ activity: { name: "music | Prefix: .", type:"LISTENING" }, status: 'online'})

});

client.login('REPLACE THIS TEXT WITH BOT TOKEN');