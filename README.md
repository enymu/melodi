# Few things before you can fully use this bot
# Hopefully you know how files and directories work

# 1) The downloaded files should be in a folder on your desktop (can be anywhere but not recommended unless
# you know what you are doing

# 2) Press your windows key (Between CTRL and ALT)

# 3) Open command prompt by typing "cmd" in the search bar and then press ENTER

# 4)Locate the folder where this file is in (If it's in your desktop skip this)

# 5)Where it says: "C:\Users\"Your PC Name">" type "cd desktop/"FOLDER YOU MADE WITH DOWNLOADED FILES""

# Now you should be where all your files for the bot is located (to make sure you can type "dir" in the command prompt

# 6) Next up, you will need to the following commands in the command prompt
# npm install discord.js
# npm install discord.js @discordjs/voice
# npm install discord.js @discordjs/opus
# npm install node.js

# 7) You will need to edit the "index.js" file.
# Open it with any text editor (like notepad) and go to the bottom
# Replace the "REPLACE THIS TEXT WITH BOT TOKEN" with a token(password/code/whatever)
# that will need to be provided by Enymu

#That should be all, you can now do "node ." or double-click the "melodi.bat" file in your folder